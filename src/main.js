"use strict";
const SPAWN_NAME = 'Spawn';
const handle_towers = require('./tower');

//#############################
//##### Creep Definitions #####
//#############################

function newRoom_resource_creep(creep) {
    //What to do
    if (creep.carry.energy === 0) {
        creep.memory.task = "harvest";
    }
    else if (creep.carry.energy === 50) {
        if (creep.memory.role === "up") {
            creep.memory.task = "upgrade";
        }
        else if (creep.memory.role === "dig") {
            creep.memory.task = "fill";
        }
        else if (creep.memory.role === "build") {
            creep.memory.task = "construct";
        }
        else if (creep.memory.role === "store") {
            creep.memory.task = "fill"
        }
    }

    //Where to do it
    if (creep.memory.task === "harvest" && !creep.memory.source) {
        let source = findEnergyOnGround(creep);
        if (source) {
            creep.memory.source = source.id;
            creep.memory.sourceType = "resource";
        }
        else {
            source = creep.pos.findClosestByPath(FIND_SOURCES_ACTIVE, {maxRooms: 1, maxOps: 500});
            if (source) {
                creep.memory.source = source.id;
                creep.memory.sourceType = "source";
            }
        }
    }
    else if (creep.memory.task === "construct" && !Game.getObjectById(creep.memory.target)) {
        let site = creep.pos.findClosestByPath(FIND_CONSTRUCTION_SITES, {maxRooms: 1, maxOps: 500});
        if (site) {
            creep.memory.target = site.id;
        }
        else {
            creep.memory.role = "store";
            delete creep.memory.source;
        }
    }
    else if (creep.memory.task === "fill" && !Game.getObjectById(creep.memory.target)) {
        let tank = creep.pos.findClosestByPath(
            FIND_MY_STRUCTURES,
            {
                filter: structure_needs_energy,
                maxRooms: 1,
                maxOps: 500
            }
        );

        tank = tank || creep.room.storage;
        
        if (tank && tank.store.getFreeCapacity(RESOURCE_ENERGY) > 0) {
            creep.memory.target = tank.id;
        }
        else if (tank) {
            creep.memory.target = null;
        }
        else {
            creep.memory.role = "dig";
            delete creep.memory.target;
        }
    }

    //Do it
    if (creep.memory.task === "harvest" && creep.memory.source) {
        let source = Game.getObjectById(creep.memory.source);
        if (creep.moveTo(source, {maxRooms: 1, maxOps: 500}) === ERR_NO_PATH) {
            creep.memory.source = null;
            return;
        }
        if (creep.pos.isNearTo(source)) {
            if (creep.memory.sourceType === "resource") {
                creep.pickup(source);
            }
            else if (creep.memory.sourceType === "source") {
                creep.harvest(source);
            }
            else {
                delete creep.memory.source;
                delete creep.memory.sourceType;
            }
        }
    }
    else if (creep.memory.task === "upgrade") {
        creep.moveTo(creep.room.controller, {maxRooms: 1, maxOps: 500});
        creep.upgradeController(creep.room.controller);
    }
    else if (creep.memory.task === "transfer") {
        creep.moveTo(Game.spawns[SPAWN_NAME], {maxRooms: 1, maxOps: 500});
        creep.transfer(Game.spawns[SPAWN_NAME], RESOURCE_ENERGY);
    }
    else if (creep.memory.task === "construct") {
        let site = Game.getObjectById(creep.memory.target);
        if (creep.moveTo(site, {maxRooms: 1, maxOps: 500}) === ERR_NO_PATH) {
            creep.memory.target = null;
            return;
        }
        creep.build(site);
    }
    else if (creep.memory.task === "fill") {
        let tank = Game.getObjectById(creep.memory.target);
        if (creep.moveTo(tank, {maxRooms: 1, maxOps: 500}) === ERR_NO_PATH) {
            creep.memory.target = null;
            return;
        }
        else if (tank && tank.store.getFreeCapacity(RESOURCE_ENERGY) === 0) {
            creep.memory.target = null;
            return;
        }
        else {
            creep.transfer(tank, RESOURCE_ENERGY);
        }
    }
}

function resource_creep(creep)
{
    if (creep.memory.role === "big") {
        let source = creep.pos.findClosestByPath(FIND_SOURCES_ACTIVE, {maxRooms: 1, maxOps: 500});
        if (source && creep.pos.isNearTo(source.pos)) {
            creep.harvest(source);
        }
        else {
            creep.moveTo(source);
        }
    }
    else if (creep.memory.role === "taxi") {
        if (creep.store.getUsedCapacity(RESOURCE_ENERGY) === 0) {
            let source = creep.room.storage;
            if (source && creep.pos.isNearTo(source.pos)) {
                creep.withdraw(source, RESOURCE_ENERGY);
            }
            else {
                creep.moveTo(source);
            }
        }
        else {
            let target = creep.pos.findClosestByPath(
                FIND_MY_STRUCTURES,
                {
                    filter: structure_needs_energy,
                    maxRooms: 1,
                    maxOps: 500
                }
            );
            if (creep.pos.isNearTo(target)) {
                creep.transfer(target, RESOURCE_ENERGY);
            }
            else {
                creep.moveTo(target);
            }
        }
    }
    else if (creep.memory.role === "swiffer") {
        if (creep.store.getUsedCapacity(RESOURCE_ENERGY) === 0) {
            let source = findEnergyOnGround(creep);
            if (source) {
                if (creep.pos.isNearTo(source)) {
                    creep.pickup(source);
                }
                else {
                    creep.moveTo(source);
                }
            }
            else {
                return;
            }
        }
        else {
            let target = creep.room.storage;
            if (creep.pos.isNearTo(target)) {
                creep.transfer(target, RESOURCE_ENERGY);
            }
            else {
                creep.moveTo(target);
            }
        }
    }
    else if (creep.memory.role === "super") {
        let target = creep.room.controller;
        let source = creep.room.storage;
        let pos = get_upgrade_pos(creep)[0];

        if (target && source && creep.pos.isEqualTo(pos)) {
            creep.withdraw(source, RESOURCE_ENERGY);
            let status = creep.upgradeController(target);
        }
        else {
            creep.moveTo(pos);
        }

        if (status != OK) {
            console.log(status);
        }
    }
    else {
        newRoom_resource_creep(creep);
    }
}

//############################
//##### Utilty Functions #####
//############################
function remove_dead_creeps()
{
    //The better way (Order log n!)
    for (let creepname in Memory.creeps) {
        if (!Game.creeps[creepname]) {
            delete Memory.creeps[creepname];
        }
    }
    //The simpelton way (Order n^2)
    //for (let creep in Memory.creeps) {
    //    if (!(creep in Object.keys(Game.creeps))) {
    //        delete Memory.creeps[creep];
    //    }
    //}
}

function structure_needs_energy(structure) {
    return
        (
            structure.structureType === STRUCTURE_EXTENSION
            || structure.structureType === STRUCTURE_SPAWN
            || structure.structureType === STRUCTURE_TOWER)
        && structure.store.getFreeCapacity(RESOURCE_ENERGY) != 0;
}

function log_creep_counts(isVerbose)
{
    if (isVerbose) {
        //Get count of each creep types
        let creepCounts = {};
        for (let creepname in Game.creeps) {
            let role = Game.creeps[creepname].memory.role;
            creepCounts[role] = creepCounts[role] + 1 || 1;
        }

        //Report creep counts
        console.log(JSON.stringify(creepCounts));
    }
}

function newRoom_spawn_creep(spawn)
{
    if (spawn.spawnCreep([MOVE, MOVE, WORK, CARRY], c_name, {dryRun: true}) === 0) {
        let c_name = "Jeff" + (Game.time % 10000);
        if (spawn.memory.crole === 2) {
            spawn.spawnCreep([MOVE, MOVE, WORK, CARRY], c_name, {memory: {role: "dig"}});
            spawn.memory.crole = 1;
        }
        else if (spawn.memory.crole === 1) {
            spawn.spawnCreep([MOVE, MOVE, WORK, CARRY], c_name, {memory: {role: "dig"}});
            spawn.memory.crole = 0;
        }
        else if (spawn.memory.crole === 0) {
            spawn.spawnCreep([MOVE, MOVE, WORK, CARRY], c_name, {memory: {role: "dig"}});
            spawn.memory.crole = 1; //Change this to 2 to make spawn creeps
        }
        else {
            spawn.memory.crole = 1;
        }
    }
}

function spawn_creep(spawn)
{
    if (spawn.room.energyCapacityAvailable < 800) {
        return newRoom_spawn_creep(spawn);
    }

    let body = {};
    let crole = "";
    let c_name = "Jeff" + (Game.time % 10000);

    if (spawn.room.find(FIND_MY_CREEPS, {filter: c => c.memory.role === "up"}).length < 1) {
        body = [MOVE, MOVE, WORK, CARRY];
        crole = "up";
    }
    else if (spawn.room.find(FIND_MY_CREEPS, {filter: c => c.memory.role === "dig"}).length < 3) {
        body = [MOVE, MOVE, WORK, CARRY];
        crole = "dig";
    }
    else if (spawn.room.find(FIND_MY_CREEPS, {filter: c => c.memory.role === "big"}).length < 2) {
        body = [WORK, WORK, WORK, WORK, WORK, MOVE, MOVE, MOVE, MOVE, MOVE];
        crole = "big";
    }
    else if (spawn.room.find(FIND_MY_CREEPS, {filter: c => c.memory.role === "up"}).length < 2) {
        body = [MOVE, MOVE, WORK, CARRY];
        crole = "up";
    }
    else if (spawn.room.find(FIND_MY_CREEPS, {filter: c => c.memory.role === "taxi"}).length < 1) {
        body = [
            CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY,
            MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE
        ];
        crole = "taxi";
    }
    else if (spawn.room.find(FIND_MY_CREEPS, {filter: c => c.memory.role === "swiffer"}).length < 4) {
        body = [
            CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY,
            MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE
        ];
        crole = "swiffer";
    }
    else if (spawn.room.find(FIND_MY_CREEPS, {filter: c => c.memory.role === "super"}).length < 1) {
        body = [
            CARRY, MOVE, WORK, WORK, WORK, WORK, WORK, WORK, WORK
        ];
        crole = "super";
    }
    else {
        return;
    }

    if (spawn.spawnCreep(body, c_name, {dryRun: true}) === 0) {
        spawn.spawnCreep(body, c_name, {memory: {role: crole}});
        console.log("Making creep of role type:", crole);
    }
}

function get_upgrade_pos(creep)
{
    let roomPos = creep.room.storage.pos;
    let controller = creep.room.controller;
    let rn = roomPos.roomName;

    let valid_pos = [];
    for (let x = roomPos.x - 1; x <= roomPos.x + 1; x++) {
        for (let y = roomPos.y - 1; y <= roomPos.y + 1; y++) {
            let newPos = new RoomPosition(x, y, rn);
            if (!newPos.isEqualTo(roomPos) && newPos.inRangeTo(controller, 3)) {
                valid_pos.push(newPos);
            }
        }
    }

    return _.filter(valid_pos, isNotOccupied);
}

function isNotOccupied(roomPos)
{
    return true;
    // TODO
    //let look = roomPos.look();
    
    //if (str_object['type'] === 'terrain') {
        //make sure we can walk on it
    //}
}

function findEnergyOnGround(creep)
{
    return creep.pos.findClosestByPath(FIND_DROPPED_RESOURCES, {filter: r => r.amount > 400, range: 1});
}

//#####################
//##### Main Loop #####
//#####################
module.exports.loop = function ()
{
    let spawn = Game.spawns[SPAWN_NAME];

    spawn_creep(spawn);

    for (let creepname in Game.creeps) {
        resource_creep(Game.creeps[creepname]);
    }

    handle_towers();

    remove_dead_creeps();
    log_creep_counts(Memory.creep_logging);
}
// Congrats Dillon
