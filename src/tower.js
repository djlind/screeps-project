function find_hurt_friend(tower) {
    return tower.pos.findClosestByRange(
        FIND_MY_CREEPS,
        {filter: c => c.hits < c.hitsMax});
}

function find_enemy(tower) {
    return tower.pos.findClosestByRange(FIND_HOSTILE_CREEPS);
}

function run_tower(tower) {
    let enemy = find_enemy(tower);
    if (enemy) {
        return tower.attack(enemy);
    }

    let hurtFriend = find_hurt_friend(tower);
    if (hurtFriend) {
        return tower.heal(hurtFriend);
    }

    return ERR_NOT_FOUND;
}

function is_ready(tower) {
    return tower && tower.store[RESOURCE_ENERGY] >= 10;
}

function run_towers(tower_ids) {
    return _.chain(tower_ids)
        .map(Game.getObjectById)
        .filter(is_ready)
        .map(run_tower)
        .value();
}

function find_towers(structures) {
    return _.chain(structures)
        .filter(s => s.structure_type === STRUCTURE_TOWER)
        .map(t => t.id)
        .value();
}

function handle_towers() {
    if (Game.tick % 100 === 0) {
        Memory.towers = find_towers(Game.structures);
    }

    return run_towers(Memory.towers || []);
}

module.exports = handle_towers;
